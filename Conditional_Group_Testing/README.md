# Conditional Group Testing

This example implements a Message Template where a Group Field may or may not be included in the test case generation process depending on the value of another Field. This example is similar to the [Conditional Group Rendering](../Conditional_Group_Rendering/README.md) example and builds on that. If you have not done so, it is highly recommended to familiarize yourself with that example first, as we will be referring to it throughout this document.

# Topics Covered

The example demonstrates multiple concepts and features using a single Project; these are:

 * Exposing Field Values in the Session
 * Using Variable Collection and Session Variables
 * Using Condition Group Collections and Condition Rules
 * Configuring conditionally rendered and tested Groups
 * Using Transforms
 * Using a Group with the Full Logic

However, we have covered most of these in the [Conditional Group Rendering](../Conditional_Group_Rendering/README.md) example. Therefore, in this example, we will focus on the last two in the list, but mainly on Using a Group with the Full Logic.

# Getting Started

Follow the instructions below to import the Project and its assets into GUARDARA.

 1. Clone this Git repository
 2. Log in to GUARDARA Manager
 3. Switch to the *Test Assets* page
 4. Click the *Import* button and select the *Import Project* option
 5. Upload the `project.zip` file from this directory

In a second, a Project named "Conditional Group Testing" appears on the *Test Assets* page. At the beginning of the Project's name, you will find a small triangle which allows you to expand the Project and view all the related assets.

In the following section we are going to focus on the Project's message template named "Conditional Group Testing".

## Message Template

Click on the *Edit* button of the *Conditional Group Testing* message template to open the Message Template Designer.

In this example we have a modified version of the Condition Group Collection and Message Template assets of the [Conditional Group Rendering](../Conditional_Group_Rendering/README.md) example. Below we discuss the modelled structured data.

 1. We have wrapped the *MyNumber*, *Separate* and *MyGroup* fields in a Group field named *Full Logic*. This wrapper group has its *Mutation Logic* property set to *Full*. This mutation logic generates all possible permutations of all child field values configured to be tested. In our example, these fields are the *MyNumber* and *TestNumber* (via *MyGroup*) fields.
 2. The *MyNumber* and *TestNumber* fields are configured to produce single-byte numeric values in binary format. They are also configured not to produce all possible values that fit a single byte (`0..255`) but use their internal algorithms to produce only values that are most likely to trigger unexpected behaviour. The main reason for setting up the fields this way is to reduce the number of test cases generated for this demonstration, making it easier to follow what is happening manually.
 3. According to the above, the *Condition Group* within the Condition Group collection of the *MyGroup* group has been updated so now the group is processed when the value of the *MyNumber* field is greater than `128`.
 4. We have applied the *Hex String* Transform on both the *MyNumber* and *TestNumber* fields. It not only allows us to demonstrate the use of Transforms but, in this case, makes it easier to see in the Preview and Engine output as the byte values are hex encoded, so the values are displayed in ASCII. For readability, we could have just set both fields' *Format* property to *ASCII*. However, in that mode, the Number field ignores the *Size* property and generates a lot more values, which would result in the generation of significantly more test cases.

### The Impact of Full Logic

In the [Conditional Group Rendering](../Conditional_Group_Rendering/README.md) example we could never get to testing the *MyString* fields value within the *MyGroup* group, even if its *Test Field* property was set to *Yes*. Check out the *Analysis* section of the documentation of the example to learn why.

In this example, however, setting the *TestNumber* field's *Test Field* property to *Yes* and the use of the *Full* logic allows us to perform testing of the *TestNumber* field whenever the *MyGroup* group is processed - when the *Conditional Rendering Rule* evaluates to true. As of this, whenever the value of the *MyNumber* field is greater than `128`, the test case generation process will perform a complete test of the *TestNumber* field for each value.

For example, **if** the *MyNumber* field generated **only** the values `0, 1, 129, 254` and the *TestNumber* field generated **only** the values `1, 2, 3`, this would yield the following test cases:

```
0|
1|
129|1<-Testing!
129|2<-Testing!
129|3<-Testing!
254|1<-Testing!
254|2<-Testing!
254|3<-Testing!
```

If you look at the Preview of the Message Template by clicking the *Preview* button in the top-right corner of the Message Template Designer, you will see that with the model implemented, the Engine will produce `2,400` test cases. Change the *Test Field* property of the *Indicator* field ("Testing!" string) to *Yes* and look at the Preview again. With this updated configuration, we get `121,470` test cases. This is because the String field generates over `2,000` values on its own. Adding that to the number of values produced by the *Test Number* field and multiplying the result with the number of values produced by the *MyNumber* field, you can see both the benefits and disadvantages of using the *Full* logic. While *Full* logic is great, we have to be reasonable with its use as it can yield an enormous amount of test cases.

### Working with Transforms

Transforms apply transformations to the values generated by the fields they are attached to. Click on the *MyNumber* field to see its properties. On the right-hand side of the screen, collapse the *Properties* section and expand the *Transform* section to see the *Hex String* transform applied to the field. You will notice that you can apply multiple transforms to a field, and the transformations will be applied in the order of appearance. Feel free to experiment with the example and add/remove transformations and use the Preview feature to see how it changes the generated test cases.

If the built-in Transforms provided by the Engine are insufficient for your use case, you can implement your custom Transforms using the SDK.

# Running the Project

Even though you already know what to expect from the test configuration during a test run, thanks to the Preview feature, you may want to see the test run.

You have to do the following to run the test:

 1. Make sure you have an Engine deployed and running.
 2. Edit the Project to ensure your Engine is selected on the *General* tab using the *Select Engine* dropdown.
 3. Make sure the Dummy driver is selected on the *General* tab.
 4. Make sure the *Debug* option of the driver is enabled by inspecting the *Connection Settings* section of the test target configuration under the *Test Targets* tab.
 5. Save the Project.
 6. Go back to the *Test Assets* page and click the *Start Test* button of the Project.
 7. Go to the *Tests Page* to see your running test.

You will also want to have a look at the terminal where you started your Engine, as it is where it will print the generated test cases due to how the *Driver* selected for the Project was configured.
