# Testable Length Fields

This example shows how to use the Sizer Field to calculate the length of a field within structured data runtime. The example also shows how to create a setup to test the implemented length field during a test run. 

# Topics Covered

The example demonstrates multiple concepts and features using a single Project; these are:

 * Using the Sizer Field
 * Using the Switch Field

# Getting Started

Follow the instructions below to import the Project and its assets into GUARDARA.

 1. Clone this Git repository
 2. Log in to GUARDARA Manager
 3. Switch to the *Test Assets* page
 4. Click the *Import* button and select the *Import Project* option
 5. Upload the `project.zip` file from this directory

In a second, a Project named "Testable Length Fields" appears on the *Test Assets* page. At the beginning of the Project's name, you will find a small triangle which allows you to expand the Project and view all the related assets.

In the following section we are going to focus on the Project's message template named "Conditional Group Testing".

# Message Template

Click on the *Edit* button of the *Conditional Group Testing* message template to open the Message Template Designer.

The example demonstrates the Sizer Field using an HTTP request template. The Sizer Field is used to calculate the HTTP request body's length and include the calculation's result as the value of the *Content-Length* HTTP header.

The *Content-Length* HTTP header is implemented by the Group Field named "content-length". Locate this Group and click on the Sizer Field named "value" set as the default value of the "Testable Size" Switch Field.

As can be seen from the *Group Name* property of the Sizer Field, it was set up to calculate the length of the Group called "body", which represents the entire request body within the HTTP request. The *Format* property was set to *ASCII* because the calculated length is expected to be an ASCII number. You may have noticed that the Sizer Field does not have a *Test Field* property. The Sizer Field always generated a valid value.

If you open the message preview by clicking the *Preview* button in the top-right corner of the screen you can see from the rendered HTTP message that the *Content-Length* header's value matches the length of the request body.

Despite the Sizer Field always generating the correct value, due to how we implemented the *Content-Length* header, the *Content-Length* header's value is still mutated. You can check this by moving the slider at the bottom of the preview window just a bit. These mutations result from making the Sizer Field the first child of the Switch Group and adding the additional String and Number fields to the Switch.

Please note that using the Sizer and Switch combo in the way presented by this example is **unsuitable for testing web applications and web services**. If the value of the *Content-Length* header indicates a request body longer than the actual request body sent, the web server will keep waiting for further data to arrive. Ultimately, at some point, the connection will time out. Unfortunately, this would negatively impact the test performance. Therefore, when testing web applications and web services, the safest is to include **only** the Sizer Field as the value of the *Content-Length* header, **without** the Switch, String and Number Fields.

In the message preview, move the slider about 3/5 from the left. At this point, the Engine moved from testing the value of the *Content-Length* header to testing the request body. You should see the request body altered and the *Content-Length* header displaying the correct length of the HTTP request body, similarly to the example below.

```
POST /example HTTP/1.1
User-Agent: GUARDARA
Host: localhost
Content-Type: application/json
Content-Length: 523
Connection: close

{"id": "|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"}
```

# Running the Project

Even though you already know what to expect from the test configuration during a test run, thanks to the Preview feature, you may want to see the test run.

You have to do the following to run the test:

 1. Make sure you have an Engine deployed and running.
 2. Edit the Project to ensure your Engine is selected on the *General* tab using the *Select Engine* dropdown.
 3. Make sure the Dummy driver is selected on the *General* tab.
 4. Make sure the *Debug* option of the driver is enabled by inspecting the *Connection Settings* section of the test target configuration under the *Test Targets* tab.
 5. Save the Project.
 6. Go back to the *Test Assets* page and click the *Start Test* button of the Project.
 7. Go to the *Tests Page* to see your running test.

You will also want to have a look at the terminal where you started your Engine, as it is where it will print the generated test cases due to how the *Driver* selected for the Project was configured.
