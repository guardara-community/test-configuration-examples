# Test Configuration Examples

Test configuration examples to demonstrate the use of the different test assets and their properties.

# Examples

| Example | Description |
| ------- | ----------- |
| [Conditional Group Rendering](./Conditional_Group_Rendering/) | This example implements a Message Template where the value of a Group Field may or may not be included (rendered) in the generated test cases depending on the value of another Field. The example demonstrates the use of Session Variables, Fields and their properties and Group Conditional Processing Rules. |
| [Conditional Group Testing](./Conditional_Group_Testing/) | This example implements a Message Template where a Group Field may or may not be included in the test case generation process depending on the value of another Field. The example build on the previous [Conditional Group Rendering](./Conditional_Group_Rendering/) example, but covers additional topics such as using the *Full* mutation logic of the Group field and using Transforms. |
| [Conditional Flow Execution](./Conditional_Flow_Execution/) | This example shows how to assign Condition Groups to Test Flow Actions to implement dynamic test flows. |
| [Configurable Field Default Value](./Configurable_Field_Default/) | This example demonstrates how you can create test configurations that can be easily customized by users when creating or editing Projects. The example demonstrates the use of the Switch and Reference Fields. |
| [Testable Length Fields](./Testable_Length_Fields/) | The example shows how to use the Sizer Field to calculate the length of a field within structured data runtime. The example also shows how to create a setup to test the implemented length field during a test run. |
| [HTTP Basic Authentication](./HTTP_Basic_Authentication/) | The example demonstrates how you can test HTTP Basic Authentication. The example can be easily adjusted to implement a password brute-forcer or to perform Bearer-token authentication. |
| [Commandline Testing](./Commandline_Testing/) | This example shows how to test commandline applications using GUARDARA. |
