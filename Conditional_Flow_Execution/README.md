# Conditional Flow Execution

This example shows how to assign Condition Groups to Test Flow Actions to implement dynamic test flows.

# Topics Covered

The example demonstrates multiple concepts that were already showcased by the other examples. The main difference with this example is that this time Condition Groups are used to control the test flow execution.

This example assumes you are already familiar with the core test configuration assets, Session Variable Collections, Condition Group Collections and how to Expose Fields in the Session.

# Getting Started

Follow the instructions below to import the Project and its assets into GUARDARA.

 1. Clone this Git repository
 2. Log in to GUARDARA Manager
 3. Switch to the *Test Assets* page
 4. Click the *Import* button and select the *Import Project* option
 5. Upload the `project.zip` file from this directory

In a second, a Project named "Conditional Flow Example" appears on the *Test Assets* page. At the beginning of the Project's name, you will find a small triangle which allows you to expand the Project and view all the related assets.

In the following section we are going to focus on the Project's Test Flow Template named "Conditional Flow".

# Test Flow Template

Open the *Conditional Flow* test flow template using the Flow Template Designer.

You can see six actions within the test flow. The first Send action has the "First Message" message template attached, with a single *Number Field* that produces numeric values. The test flow has been set up so that the second pair of *Send* and *Receive* actions are only executed if the *Number Field*'s value is greater than `1024` and less than `65535`.

This behaviour was achieved by:

 1. Exposing the Number Field in the session via a Session Variable defined in the Project's Variable Collection.
 2. Assigning a Condition Group defined in the Projects Condition Group Collection to the last Send and Receive actions.

# Running the Project

Follow the instructions below to run the test:

 1. Make sure you have an Engine deployed and running.
 2. Edit the Project to ensure your Engine is selected on the *General* tab using the *Select Engine* dropdown.
 3. Make sure the Dummy driver is selected on the *General* tab.
 4. Make sure the *Debug* option of the driver is enabled by inspecting the *Connection Settings* section of the test target configuration under the *Test Targets* tab.
 5. Save the Project.
 6. Go back to the *Test Assets* page and click the *Start Test* button of the Project.
 7. Go to the *Tests Page* to see your running test.

You will also want to have a look at the terminal where you started your Engine, as it is where it will print the generated test cases due to how the *Driver* selected for the Project was configured.
