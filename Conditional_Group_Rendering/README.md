# Conditional Group Rendering

This example implements a Message Template where the value of a Group Field may or may not be included in the generated test cases depending on the value of another Field.

# Topics Covered

The example demonstrates multiple concepts and features using a single Project; these are:

 * Exposing Field Values in the Session
 * Using Variable Collection and Session Variables
 * Using Condition Group Collections and Condition Rules
 * Configuring conditionally rendered Groups

# Getting Started

Follow the instructions below to import the Project and its assets into GUARDARA.

 1. Clone this Git repository
 2. Log in to GUARDARA Manager
 3. Switch to the *Test Assets* page
 4. Click the *Import* button and select the *Import Project* option
 5. Upload the `project.zip` file from this directory

In a second, a Project named "Conditional Group Rendering" appears on the *Test Assets* page. At the beginning of the Project's name, you will find a small triangle which allows you to expand the Project and view all the related assets.

# Project Assets

Other than the Project itself, there are three assets implemented, these are:

| Name | Type | Description |
| ------------------------------- | ------------------- | ----------- |
| Conditional Rendering Variables | Variable Collection | A Variable Collection containing a single hidden Session Variable in which the `MyNumber` Field in the Message Template will expose its value. |
| Conditional Group Rendering | Condition Group Collection | A Condition Group Collection containing a condition group that defines a single rule to apply on the Group field within the message template named "Conditional Rendering Message". |
| Conditional Rendering Message | Message Template | The Message template contains the `MyNumber` Field and a Group Field that was set up, so it is only processed if the value of the `MyNumber` field is greater than `31337`. |
| Conditional Rendering Flow | Test Flow Template | A simple Test Flow template with the *Conditional Rendering Message* template assigned to its *Send* action. This is provided so you can run a Dummy test and see the generated test cases on the Engine console. |

Finally, we have the Project. The Project was configured to use the Dummy driver in debug mode, so it prints all test cases to the Engine console. You will have to set up an Engine before running the test, but more on that later.

In the following sections we are going to discuss the key assets in more detail.

## Variable Collection

You can use a Variable Collection to define one or more Session Variables. You can use Session Variables to share information between Fields, Response Processing Rules, Callbacks, Flow Templates and the Project. The Session Variables, and therefore the Variable Collection, is a key ingredient of stateful test configurations.

If you click on the *Edit* button of the *Conditional Rendering Variables* variable collection, you land on the Variable Collection Designer. There are three tabs listing the different types of variables. Under the *Hidden* tab, you can find the Session Variable called *MyNumber*. The variable was created as a *Hidden* variable because only the test configuration will interact with it. Users do not need to see the variable listed in the Project configuration or change its value.

## Condition Group Collection

A Condition Group Collections allows you to define a set of Condition Groups, each with its list of rules. These Condition Groups can be assigned to the Group Field within Message templates or Flow Actions within Test Flow templates. The Group Field or Flow Action the Condition Group is attached to is processed only when all the rules within the Condition Group evaluate to be true.

The Condition Group Collection in this example has a Variable Collection assigned. This allows the creation of rules that are evaluated based on the value of a Session Variable.

Let's move on to discussing the Message Template to see how the *MyNumber* variable is used.

## Message Template

Click on the *Edit* button of the *Conditional Rendering Message* template to open the Message Template Designer.

Before we discuss the structured data model implemented, let's look at how the previously discussed Variable Collection was assigned to the Message Template.

### Variable Collection Assignment

Select the *View > Configuration* from the Editor Menu right under the Message Template name to see the basic configuration options. You can use the select field named *Variable Collection* to assign a Variable Collection to the template. As you can see, we already have our Variable Collection assigned, so we do not have to do anything this time. Close the configuration panel.

### Condition Group Collection Assignment

Select the *View > Configuration* from the Editor Menu right under the Message Template name to see the basic configuration options. You can use the select field named *Condition Group Collection* to assign a Condition Group Collection to the template. As you can see, we already have our Condition Group Collection assigned, so we do not have to do anything this time. You can close the configuration panel.

### Modelled Structured Data

You can see the visual representation of the modelled data structure in the Workspace at the centre of the screen.

As can also be seen from the Layout on the left-hand side of the screen, we have three Fields on the root level. The previously mentioned *MyNumber* numeric field, a delimiter field and a Group field that contains a String field.

#### MyNumber Field

Click the *MyNumber* field in the Layout tab or the Workspace to see the properties of the field. The properties editor will be displayed on the right-hand side of the screen.

Scroll down a bit until you find the *Show advanced* checkbox at the end of the property list and check the checkbox to see the advanced properties of the field. As you can see, the *Expose in Session* property of the field is set to *Yes* and the *Expose as...* property is set to the hidden Session Variable configured in the Variable Collection. With this configuration, we ensure that the field's actual value can be accessed using the variable when the test runs.

#### MyGroup Field

The following field of interest is the Group field called *MyGroup*. You may have noticed a checkbox at the top-right corner of the field with the `+` sign to its right. This indicates that a Condition Group has been assigned to the field. Check the checkbox to see the assigned rule: `Block Rendering`. This rule tells the Engine to consider this Group only if the value of the *MyNumber* field becomes greater than `31337` during the test run. You can have a look at this rule by opening the "Conditional Group Rendering" Condition Group Collection on the *Test Assets* page.

#### Preview

Given that the default value of the *MyNumber* field is `1` (represented as an ASCII number), the *MyGroup* field and its children are not visible by default. You can check this by clicking the *Preview* button on the top-right corner of the screen. You can see that the default value of the structured data model renders to the value `1|`. The `1` part of this value comes from the *MyNumber* field and the `|` character from the delimiter field called *Separate*. What is missing is the "Greater than 31337!" string. Move the slider at the bottom of the Preview window halfway and see how that changes the Preview. In my case, for example, close to half of the slider, I get the value `64424509441|Greater than 31337!` rendered. Indeed, `64424509441` is greater than `31337`.

# Running the Project

Even though you already know what to expect from the test configuration during a test run, thanks to the Preview feature, you may want to see the test run.

You have to do the following to run the test:

 1. Make sure you have an Engine deployed and running.
 2. Edit the Project to ensure your Engine is selected on the *General* tab using the *Select Engine* dropdown.
 3. Make sure the Dummy driver is selected on the *General* tab.
 4. Make sure the *Debug* option of the driver is enabled by inspecting the *Connection Settings* section of the test target configuration under the *Test Targets* tab.
 5. Save the Project.
 6. Go back to the *Test Assets* page and click the *Start Test* button of the Project.
 7. Go to the *Tests Page* to see your running test.

You will also want to have a look at the terminal where you started your Engine, as it is where it will print the generated test cases due to how the *Driver* selected for the Project was configured.

# Analysis

There are a few test configuration related details worth highlighting:

 * The *Condition Group* configured for the *MyGroup* group field ensures that the group is only processed during the test if the value of the *MyNumber* field is greater than `31337`.
 * The default value of the *MyNumber* field is `1`. During the test run the field is going to change its value several times. However, once the testing of the field is done, it will revert to the default value.

There is a Root group which is not visible in the Message Template Designer. Every field you add to the Message Template becomes the child of this Root group directly or indirectly through another group. This Root group is configured to use the **Linear mutation logic**. A Group utilising the Linear logic tests its **direct** children in the order of their appearance, always one child at a time. After a child's testing is completed, the child reverts to its default, and the testing process continues with the following field. The fields already tested will not be changed again.

Therefore, after the testing of the *MyNumber* field is done:

 * The *MyGroup* field will not show up again
 * As the following field in the line is the *MyGroup* field and is not there due to the default value of the *MyNumber* field, the test ends.

If we wanted to see how our target handled test cases where the value of the *MyNumber* field was greater than `31337`, and we also generated test cases based on the *MyString* field (e.g.: `65535|AAAAAAAAAAAAA`), we would have to:

 * Utilise a Group Field with the Full logic set
 * Set the *Test Field* property of the *MyString* field to *Yes*
 
We discuss this scenario in the [Conditional Group Testing](../Conditional_Group_Testing/) example. 

# What's Next

The best way to learn is by trying. Play around with the example and try a few scenarios, such as:

 * Changing Field properties
 * Rearranging fields
 * Set non-tested fields to be tested

Once you have had enough, you may also want to check out the other examples.
